package at.kelut.kosmetikkuchl.kosmetikkuchl

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val items = listOf<String>(
            "Butter",
            "Eiern",
            "Milch",
            "Schokolade",
            "Mandeln",
            "Natron",
            "weißer Zucker",
            "brauner Zucker"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var ingredientsList = findViewById<RecyclerView>(R.id.ingredients_list)
        ingredientsList.layoutManager = LinearLayoutManager(this)
        ingredientsList.adapter = IngredientsListAdapter(items)


    }
}
